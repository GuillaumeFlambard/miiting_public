/**
 * Automatically generated file. DO NOT MODIFY
 */
package fr.miiting.app;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "fr.miiting.app";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 829;
  public static final String VERSION_NAME = "0.8.29";
}
