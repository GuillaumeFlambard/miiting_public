/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.google.android.exoplayer2;

public final class R {
    private R() {}

    public static final class attr {
        private attr() {}

        public static final int ad_marker_color = 0x7f020025;
        public static final int ad_marker_width = 0x7f020026;
        public static final int auto_show = 0x7f020038;
        public static final int bar_height = 0x7f020040;
        public static final int buffered_color = 0x7f020042;
        public static final int controller_layout_id = 0x7f020082;
        public static final int default_artwork = 0x7f020085;
        public static final int fastforward_increment = 0x7f02009a;
        public static final int font = 0x7f02009c;
        public static final int fontProviderAuthority = 0x7f02009e;
        public static final int fontProviderCerts = 0x7f02009f;
        public static final int fontProviderFetchStrategy = 0x7f0200a0;
        public static final int fontProviderFetchTimeout = 0x7f0200a1;
        public static final int fontProviderPackage = 0x7f0200a2;
        public static final int fontProviderQuery = 0x7f0200a3;
        public static final int fontStyle = 0x7f0200a4;
        public static final int fontWeight = 0x7f0200a5;
        public static final int hide_during_ads = 0x7f0200aa;
        public static final int hide_on_touch = 0x7f0200ab;
        public static final int keep_content_on_player_reset = 0x7f0200b9;
        public static final int played_ad_marker_color = 0x7f0200da;
        public static final int played_color = 0x7f0200db;
        public static final int player_layout_id = 0x7f0200dc;
        public static final int repeat_toggle_modes = 0x7f0200ed;
        public static final int resize_mode = 0x7f0200ee;
        public static final int rewind_increment = 0x7f0200f1;
        public static final int scrubber_color = 0x7f020101;
        public static final int scrubber_disabled_size = 0x7f020102;
        public static final int scrubber_dragged_size = 0x7f020103;
        public static final int scrubber_drawable = 0x7f020104;
        public static final int scrubber_enabled_size = 0x7f020105;
        public static final int show_buffering = 0x7f020110;
        public static final int show_shuffle_button = 0x7f020111;
        public static final int show_timeout = 0x7f020112;
        public static final int shutter_background_color = 0x7f020113;
        public static final int surface_type = 0x7f020122;
        public static final int touch_target_height = 0x7f02014b;
        public static final int unplayed_color = 0x7f02015f;
        public static final int use_artwork = 0x7f020160;
        public static final int use_controller = 0x7f020161;
    }
    public static final class bool {
        private bool() {}

        public static final int abc_action_bar_embed_tabs = 0x7f030000;
    }
    public static final class color {
        private color() {}

        public static final int exo_edit_mode_background_color = 0x7f040052;
        public static final int exo_error_message_background_color = 0x7f040053;
        public static final int notification_action_color_filter = 0x7f040064;
        public static final int notification_icon_bg_color = 0x7f040065;
        public static final int notification_material_background_media_default_color = 0x7f040066;
        public static final int primary_text_default_material_dark = 0x7f04006c;
        public static final int ripple_material_light = 0x7f040071;
        public static final int secondary_text_default_material_dark = 0x7f040072;
        public static final int secondary_text_default_material_light = 0x7f040073;
    }
    public static final class dimen {
        private dimen() {}

        public static final int compat_button_inset_horizontal_material = 0x7f050063;
        public static final int compat_button_inset_vertical_material = 0x7f050064;
        public static final int compat_button_padding_horizontal_material = 0x7f050065;
        public static final int compat_button_padding_vertical_material = 0x7f050066;
        public static final int compat_control_corner_material = 0x7f050067;
        public static final int exo_media_button_height = 0x7f05006a;
        public static final int exo_media_button_width = 0x7f05006b;
        public static final int notification_action_icon_size = 0x7f050073;
        public static final int notification_action_text_size = 0x7f050074;
        public static final int notification_big_circle_margin = 0x7f050075;
        public static final int notification_content_margin_start = 0x7f050076;
        public static final int notification_large_icon_height = 0x7f050077;
        public static final int notification_large_icon_width = 0x7f050078;
        public static final int notification_main_column_padding_top = 0x7f050079;
        public static final int notification_media_narrow_margin = 0x7f05007a;
        public static final int notification_right_icon_size = 0x7f05007b;
        public static final int notification_right_side_padding_top = 0x7f05007c;
        public static final int notification_small_icon_background_padding = 0x7f05007d;
        public static final int notification_small_icon_size_as_large = 0x7f05007e;
        public static final int notification_subtext_size = 0x7f05007f;
        public static final int notification_top_pad = 0x7f050080;
        public static final int notification_top_pad_large_text = 0x7f050081;
    }
    public static final class drawable {
        private drawable() {}

        public static final int exo_controls_fastforward = 0x7f0600d8;
        public static final int exo_controls_fullscreen_enter = 0x7f0600d9;
        public static final int exo_controls_fullscreen_exit = 0x7f0600da;
        public static final int exo_controls_next = 0x7f0600db;
        public static final int exo_controls_pause = 0x7f0600dc;
        public static final int exo_controls_play = 0x7f0600dd;
        public static final int exo_controls_previous = 0x7f0600de;
        public static final int exo_controls_repeat_all = 0x7f0600df;
        public static final int exo_controls_repeat_off = 0x7f0600e0;
        public static final int exo_controls_repeat_one = 0x7f0600e1;
        public static final int exo_controls_rewind = 0x7f0600e2;
        public static final int exo_controls_shuffle = 0x7f0600e3;
        public static final int exo_edit_mode_logo = 0x7f0600e4;
        public static final int exo_icon_fastforward = 0x7f0600e5;
        public static final int exo_icon_next = 0x7f0600e6;
        public static final int exo_icon_pause = 0x7f0600e7;
        public static final int exo_icon_play = 0x7f0600e8;
        public static final int exo_icon_previous = 0x7f0600e9;
        public static final int exo_icon_rewind = 0x7f0600ea;
        public static final int exo_icon_stop = 0x7f0600eb;
        public static final int exo_notification_fastforward = 0x7f0600ec;
        public static final int exo_notification_next = 0x7f0600ed;
        public static final int exo_notification_pause = 0x7f0600ee;
        public static final int exo_notification_play = 0x7f0600ef;
        public static final int exo_notification_previous = 0x7f0600f0;
        public static final int exo_notification_rewind = 0x7f0600f1;
        public static final int exo_notification_small_icon = 0x7f0600f2;
        public static final int exo_notification_stop = 0x7f0600f3;
        public static final int notification_action_background = 0x7f060106;
        public static final int notification_bg = 0x7f060107;
        public static final int notification_bg_low = 0x7f060108;
        public static final int notification_bg_low_normal = 0x7f060109;
        public static final int notification_bg_low_pressed = 0x7f06010a;
        public static final int notification_bg_normal = 0x7f06010b;
        public static final int notification_bg_normal_pressed = 0x7f06010c;
        public static final int notification_icon_background = 0x7f06010d;
        public static final int notification_template_icon_bg = 0x7f06010e;
        public static final int notification_template_icon_low_bg = 0x7f06010f;
        public static final int notification_tile_bg = 0x7f060110;
        public static final int notify_panel_notification_icon_bg = 0x7f060111;
    }
    public static final class id {
        private id() {}

        public static final int action0 = 0x7f070008;
        public static final int action_container = 0x7f070010;
        public static final int action_divider = 0x7f070012;
        public static final int action_image = 0x7f070013;
        public static final int action_text = 0x7f070019;
        public static final int actions = 0x7f07001a;
        public static final int always = 0x7f070021;
        public static final int async = 0x7f070022;
        public static final int blocking = 0x7f070028;
        public static final int cancel_action = 0x7f07002e;
        public static final int chronometer = 0x7f070035;
        public static final int end_padder = 0x7f07004b;
        public static final int exo_artwork = 0x7f07004c;
        public static final int exo_buffering = 0x7f07004d;
        public static final int exo_content_frame = 0x7f07004e;
        public static final int exo_controller = 0x7f07004f;
        public static final int exo_controller_placeholder = 0x7f070050;
        public static final int exo_duration = 0x7f070051;
        public static final int exo_error_message = 0x7f070052;
        public static final int exo_ffwd = 0x7f070053;
        public static final int exo_next = 0x7f070054;
        public static final int exo_overlay = 0x7f070055;
        public static final int exo_pause = 0x7f070056;
        public static final int exo_play = 0x7f070057;
        public static final int exo_position = 0x7f070059;
        public static final int exo_prev = 0x7f07005a;
        public static final int exo_progress = 0x7f07005b;
        public static final int exo_repeat_toggle = 0x7f07005c;
        public static final int exo_rew = 0x7f07005d;
        public static final int exo_shuffle = 0x7f07005e;
        public static final int exo_shutter = 0x7f07005f;
        public static final int exo_subtitles = 0x7f070060;
        public static final int exo_track_selection_view = 0x7f070061;
        public static final int fill = 0x7f070064;
        public static final int fit = 0x7f070065;
        public static final int fixed_height = 0x7f07006b;
        public static final int fixed_width = 0x7f07006c;
        public static final int forever = 0x7f07006e;
        public static final int icon = 0x7f070073;
        public static final int icon_group = 0x7f070074;
        public static final int info = 0x7f07007d;
        public static final int italic = 0x7f07007f;
        public static final int line1 = 0x7f070086;
        public static final int line3 = 0x7f070087;
        public static final int media_actions = 0x7f07008d;
        public static final int never = 0x7f070094;
        public static final int none = 0x7f070096;
        public static final int normal = 0x7f070097;
        public static final int notification_background = 0x7f070098;
        public static final int notification_main_column = 0x7f070099;
        public static final int notification_main_column_container = 0x7f07009a;
        public static final int right_icon = 0x7f0700ac;
        public static final int right_side = 0x7f0700ad;
        public static final int spherical_view = 0x7f0700cf;
        public static final int status_bar_latest_event_content = 0x7f0700d8;
        public static final int surface_view = 0x7f0700db;
        public static final int text = 0x7f0700dd;
        public static final int text2 = 0x7f0700de;
        public static final int texture_view = 0x7f0700e3;
        public static final int time = 0x7f0700e4;
        public static final int title = 0x7f0700e5;
        public static final int when_playing = 0x7f0700f7;
        public static final int zoom = 0x7f0700ff;
    }
    public static final class integer {
        private integer() {}

        public static final int cancel_button_image_alpha = 0x7f080002;
        public static final int status_bar_notification_info_maxnum = 0x7f080005;
    }
    public static final class layout {
        private layout() {}

        public static final int exo_list_divider = 0x7f090020;
        public static final int exo_playback_control_view = 0x7f090021;
        public static final int exo_player_control_view = 0x7f090022;
        public static final int exo_player_view = 0x7f090023;
        public static final int exo_simple_player_view = 0x7f090024;
        public static final int exo_track_selection_dialog = 0x7f090025;
        public static final int notification_action = 0x7f09002e;
        public static final int notification_action_tombstone = 0x7f09002f;
        public static final int notification_media_action = 0x7f090030;
        public static final int notification_media_cancel_action = 0x7f090031;
        public static final int notification_template_big_media = 0x7f090032;
        public static final int notification_template_big_media_custom = 0x7f090033;
        public static final int notification_template_big_media_narrow = 0x7f090034;
        public static final int notification_template_big_media_narrow_custom = 0x7f090035;
        public static final int notification_template_custom_big = 0x7f090036;
        public static final int notification_template_icon_group = 0x7f090037;
        public static final int notification_template_lines_media = 0x7f090038;
        public static final int notification_template_media = 0x7f090039;
        public static final int notification_template_media_custom = 0x7f09003a;
        public static final int notification_template_part_chronometer = 0x7f09003b;
        public static final int notification_template_part_time = 0x7f09003c;
    }
    public static final class string {
        private string() {}

        public static final int exo_controls_fastforward_description = 0x7f0c0061;
        public static final int exo_controls_fullscreen_description = 0x7f0c0062;
        public static final int exo_controls_next_description = 0x7f0c0063;
        public static final int exo_controls_pause_description = 0x7f0c0064;
        public static final int exo_controls_play_description = 0x7f0c0065;
        public static final int exo_controls_previous_description = 0x7f0c0066;
        public static final int exo_controls_repeat_all_description = 0x7f0c0067;
        public static final int exo_controls_repeat_off_description = 0x7f0c0068;
        public static final int exo_controls_repeat_one_description = 0x7f0c0069;
        public static final int exo_controls_rewind_description = 0x7f0c006a;
        public static final int exo_controls_shuffle_description = 0x7f0c006b;
        public static final int exo_controls_stop_description = 0x7f0c006c;
        public static final int exo_download_completed = 0x7f0c006d;
        public static final int exo_download_description = 0x7f0c006e;
        public static final int exo_download_downloading = 0x7f0c006f;
        public static final int exo_download_failed = 0x7f0c0070;
        public static final int exo_download_notification_channel_name = 0x7f0c0071;
        public static final int exo_download_removing = 0x7f0c0072;
        public static final int exo_item_list = 0x7f0c0073;
        public static final int exo_track_bitrate = 0x7f0c0074;
        public static final int exo_track_mono = 0x7f0c0075;
        public static final int exo_track_resolution = 0x7f0c0076;
        public static final int exo_track_selection_auto = 0x7f0c0077;
        public static final int exo_track_selection_none = 0x7f0c0078;
        public static final int exo_track_selection_title_audio = 0x7f0c0079;
        public static final int exo_track_selection_title_text = 0x7f0c007a;
        public static final int exo_track_selection_title_video = 0x7f0c007b;
        public static final int exo_track_stereo = 0x7f0c007c;
        public static final int exo_track_surround = 0x7f0c007d;
        public static final int exo_track_surround_5_point_1 = 0x7f0c007e;
        public static final int exo_track_surround_7_point_1 = 0x7f0c007f;
        public static final int exo_track_unknown = 0x7f0c0080;
        public static final int status_bar_notification_info_overflow = 0x7f0c0092;
    }
    public static final class style {
        private style() {}

        public static final int ExoMediaButton = 0x7f0d00b0;
        public static final int ExoMediaButton_FastForward = 0x7f0d00b1;
        public static final int ExoMediaButton_Next = 0x7f0d00b2;
        public static final int ExoMediaButton_Pause = 0x7f0d00b3;
        public static final int ExoMediaButton_Play = 0x7f0d00b4;
        public static final int ExoMediaButton_Previous = 0x7f0d00b5;
        public static final int ExoMediaButton_Rewind = 0x7f0d00b6;
        public static final int ExoMediaButton_Shuffle = 0x7f0d00b7;
        public static final int TextAppearance_Compat_Notification = 0x7f0d0125;
        public static final int TextAppearance_Compat_Notification_Info = 0x7f0d0126;
        public static final int TextAppearance_Compat_Notification_Info_Media = 0x7f0d0127;
        public static final int TextAppearance_Compat_Notification_Line2 = 0x7f0d0128;
        public static final int TextAppearance_Compat_Notification_Line2_Media = 0x7f0d0129;
        public static final int TextAppearance_Compat_Notification_Media = 0x7f0d012a;
        public static final int TextAppearance_Compat_Notification_Time = 0x7f0d012b;
        public static final int TextAppearance_Compat_Notification_Time_Media = 0x7f0d012c;
        public static final int TextAppearance_Compat_Notification_Title = 0x7f0d012d;
        public static final int TextAppearance_Compat_Notification_Title_Media = 0x7f0d012e;
        public static final int Widget_Compat_NotificationActionContainer = 0x7f0d019f;
        public static final int Widget_Compat_NotificationActionText = 0x7f0d01a0;
    }
    public static final class styleable {
        private styleable() {}

        public static final int[] AspectRatioFrameLayout = { 0x7f0200ee };
        public static final int AspectRatioFrameLayout_resize_mode = 0;
        public static final int[] DefaultTimeBar = { 0x7f020025, 0x7f020026, 0x7f020040, 0x7f020042, 0x7f0200da, 0x7f0200db, 0x7f020101, 0x7f020102, 0x7f020103, 0x7f020104, 0x7f020105, 0x7f02014b, 0x7f02015f };
        public static final int DefaultTimeBar_ad_marker_color = 0;
        public static final int DefaultTimeBar_ad_marker_width = 1;
        public static final int DefaultTimeBar_bar_height = 2;
        public static final int DefaultTimeBar_buffered_color = 3;
        public static final int DefaultTimeBar_played_ad_marker_color = 4;
        public static final int DefaultTimeBar_played_color = 5;
        public static final int DefaultTimeBar_scrubber_color = 6;
        public static final int DefaultTimeBar_scrubber_disabled_size = 7;
        public static final int DefaultTimeBar_scrubber_dragged_size = 8;
        public static final int DefaultTimeBar_scrubber_drawable = 9;
        public static final int DefaultTimeBar_scrubber_enabled_size = 10;
        public static final int DefaultTimeBar_touch_target_height = 11;
        public static final int DefaultTimeBar_unplayed_color = 12;
        public static final int[] FontFamily = { 0x7f02009e, 0x7f02009f, 0x7f0200a0, 0x7f0200a1, 0x7f0200a2, 0x7f0200a3 };
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] FontFamilyFont = { 0x7f02009c, 0x7f0200a4, 0x7f0200a5 };
        public static final int FontFamilyFont_font = 0;
        public static final int FontFamilyFont_fontStyle = 1;
        public static final int FontFamilyFont_fontWeight = 2;
        public static final int[] PlayerControlView = { 0x7f020082, 0x7f02009a, 0x7f0200ed, 0x7f0200f1, 0x7f020111, 0x7f020112 };
        public static final int PlayerControlView_controller_layout_id = 0;
        public static final int PlayerControlView_fastforward_increment = 1;
        public static final int PlayerControlView_repeat_toggle_modes = 2;
        public static final int PlayerControlView_rewind_increment = 3;
        public static final int PlayerControlView_show_shuffle_button = 4;
        public static final int PlayerControlView_show_timeout = 5;
        public static final int[] PlayerView = { 0x7f020038, 0x7f020082, 0x7f020085, 0x7f02009a, 0x7f0200aa, 0x7f0200ab, 0x7f0200b9, 0x7f0200dc, 0x7f0200ed, 0x7f0200ee, 0x7f0200f1, 0x7f020110, 0x7f020111, 0x7f020112, 0x7f020113, 0x7f020122, 0x7f020160, 0x7f020161 };
        public static final int PlayerView_auto_show = 0;
        public static final int PlayerView_controller_layout_id = 1;
        public static final int PlayerView_default_artwork = 2;
        public static final int PlayerView_fastforward_increment = 3;
        public static final int PlayerView_hide_during_ads = 4;
        public static final int PlayerView_hide_on_touch = 5;
        public static final int PlayerView_keep_content_on_player_reset = 6;
        public static final int PlayerView_player_layout_id = 7;
        public static final int PlayerView_repeat_toggle_modes = 8;
        public static final int PlayerView_resize_mode = 9;
        public static final int PlayerView_rewind_increment = 10;
        public static final int PlayerView_show_buffering = 11;
        public static final int PlayerView_show_shuffle_button = 12;
        public static final int PlayerView_show_timeout = 13;
        public static final int PlayerView_shutter_background_color = 14;
        public static final int PlayerView_surface_type = 15;
        public static final int PlayerView_use_artwork = 16;
        public static final int PlayerView_use_controller = 17;
    }
}
